<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            new RaulFraile\Bundle\LadybugBundle\RaulFraileLadybugBundle(),
            new Lexik\Bundle\FormFilterBundle\LexikFormFilterBundle(),
            new JordiLlonch\Bundle\CrudGeneratorBundle\JordiLlonchCrudGeneratorBundle(),
            new Siphoc\PdfBundle\SiphocPdfBundle(),

            new Curso\ahurtadoBundle\CursoahurtadoBundle(),
            new Curso\BastarDoBundle\CursoBastarDoBundle(),
            new Curso\DavidBundle\CursoDavidBundle(),
            new Curso\FelipeBundle\FelipeBundle(),
            new Curso\montiBundle\CursomontiBundle(),
            new Curso\SoriBundle\CursoSoriBundle(),
            new Curso\ToDoBundle\CursoToDoBundle(),
            new Curso\TxominBundle\CursoTxominBundle(),
            new Curso\XaviBundle\CursoXaviBundle(),
            new Curso\Carles9000Bundle\CursoCarles9000Bundle()

        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Acme\DemoBundle\AcmeDemoBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
