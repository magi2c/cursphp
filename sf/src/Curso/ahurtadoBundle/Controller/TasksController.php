<?php

namespace Curso\ahurtadoBundle\Controller;

use Curso\ahurtadoBundle\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TasksController extends Controller
{

    private $tasks = array();

    private function carregaTasksAction()
    {
        /*$this->tasks[0]=array("nom"=>"Tasca 1","desc"=>"Descripcio Tasca 1","pri"=>"Alta");
        $this->tasks[1]=array("nom"=>"Tasca 2","desc"=>"Descripcio Tasca 2","pri"=>"Alta");
        $this->tasks[2]=array("nom"=>"Tasca 3","desc"=>"Descripcio Tasca 3","pri"=>"Baixa");
        $this->tasks[3]=array("nom"=>"Tasca 4","desc"=>"Descripcio Tasca 4","pri"=>"Mitja");*/
    }

    public function indexAction()
    {

        //$this->carregaTasksAction();
        $em = $this->container->get('doctrine')->getManager();
        $repository = $em->getRepository('CursoahurtadoBundle:Task');
        $tasks = $repository->findAll();
        //return $this->render('CursoahurtadoBundle:Tasks:tasks.html.twig', array('tasks' => $this->tasks));
        return $this->render('CursoahurtadoBundle:Tasks:tasks.html.twig', array('tasks' => $tasks));
    }
    public function viewAction($id)
    {
        //$this->carregaTasksAction();
        $em = $this->container->get('doctrine')->getManager();
        $repository = $em->getRepository('CursoahurtadoBundle:Task');
        $task = $repository->find($id);
        return $this->render('CursoahurtadoBundle:Tasks:viewtask.html.twig', array('task'=>$task));
    }
    public function newtaskAction(Request $request)
    {
        $task = new Task();
        $form = $this->createFormBuilder($task)
        ->add('nombre', 'text')
        ->add('descripcio', 'text')
        ->add('user', 'entity', array(
            'class' => 'CursoahurtadoBundle:User',
            'property' => 'email',
            ))
        ->add('save', 'submit')
        ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em=$this->container->get('doctrine')->getManager();
            $repository = $em->getRepository('CursoahurtadoBundle:Task');
            $em->persist($task);
            $em->flush();

            return $this->redirect($this->generateUrl('cursoahurtado_tasks'));

        }

        return $this->render('CursoahurtadoBundle:Tasks:newtasks.html.twig', array('form'=>$form->createView()));



    }
}



