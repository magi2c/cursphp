<?php

namespace Curso\ahurtadoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TasksController extends Controller
{

    private $tasks = array();

    private function carregaTasksAction()
    {
        $this->tasks[0]=array("nom"=>"Tasca 1","desc"=>"Descripcio Tasca 1","pri"=>"Alta");
        $this->tasks[1]=array("nom"=>"Tasca 2","desc"=>"Descripcio Tasca 2","pri"=>"Alta");
        $this->tasks[2]=array("nom"=>"Tasca 3","desc"=>"Descripcio Tasca 3","pri"=>"Baixa");
        $this->tasks[3]=array("nom"=>"Tasca 4","desc"=>"Descripcio Tasca 4","pri"=>"Mitja");
    }

    public function indexAction()
    {
        $this->carregaTasksAction();
        return $this->render('CursoahurtadoBundle:Tasks:tasks.html.twig', array('tasks' => $this->tasks));
    }
    public function viewAction($id)
    {
        $this->carregaTasksAction();
        return $this->render('CursoahurtadoBundle:Tasks:viewtask.html.twig', array('task'=>$this->tasks[$id], 'id'=>$id));
    }

}



