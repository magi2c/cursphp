<?php

namespace Curso\ahurtadoBundle\Command;

use Curso\ahurtadoBundle\Entity\Task;
use Curso\ahurtadoBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class TaskCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('curso:ahmaddtasks')
            ->setDescription('Create tasks')
            ->addArgument('num', InputArgument::REQUIRED, 'Numero de tascas creadas')
            /*->addArgument('Iduser', InputArgument::REQUIRED, 'Id Usuario al que se le asigna')*/
            ->addArgument('mailuser', InputArgument::REQUIRED, 'Mail Usuario al que se le asigna')
            ->addOption('sufix', 'u', InputOption::VALUE_OPTIONAL, 'Sufix task')
            ->setHelp(<<<EOF
The <info>%command.name%</info> :

<info>php %command.full_name%</info>

Inserta task en la bd


<info>php %command.full_name%</info> 10
EOF
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $numTask = $input->getArgument('num');
        $Mailuser = $input->getArgument('mailuser');
        $em = $this->getContainer()->get('doctrine')->getManager();
        $output->writeln('Creamos <comment>'.$numTask.'</comment>!...'.$input->getOption('sufix'));
        $repository = $em->getRepository('CursoahurtadoBundle:User');
        $user = $repository->findOneByEmail($Mailuser);

        if (is_null($user)){
            $user = new User();
            $user->setEmail( $input->getArgument('mailuser') );
            $user->setNom('Test');
            $em = $this->getContainer()->get('doctrine')->getManager();
            $em->persist( $user );
            $em->flush();
            $output->writeln( "Usuari creat:\nNom: ".$user->getNom()."\nemail: ".$user->getEmail()."\nid: ". $user->getId() );
        }

        for ($i = 1; $i <= $numTask; $i++) {
            $task = new Task();
            $task->setNombre('test '.$i);
            $task->setUser($user);
            $task->setDescripcio('Descripicio Tasca '.$i);
            $em->persist($task);
        }
        $em->flush();

    }
}
