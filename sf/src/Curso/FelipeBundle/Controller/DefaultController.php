<?php

namespace Curso\FelipeBundle\Controller;

use Curso\FelipeBundle\Entity\Tarea;
use Curso\FelipeBundle\Entity\Usuari;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;


class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('FelipeBundle:Default:index.html.twig', array('name' => $name));
    }


    public function variableOpAction($slug)
    {
        die('mi propio routing + '.$slug);
    }


private $tasks = array(
          '1' =>  array('numero' => 1, 'prioridad' => 'alta', 'descripcion' => 'Tarea de almacenamiento', 'desclarga' =>'Esta es la descripcion larga de la tarea 1'),
          '2' =>  array('numero' => 2, 'prioridad' => 'baja', 'descripcion' => 'Tarea de subida', 'desclarga' =>'Esta es la descripcion larga de la tarea 2'),
          '3' =>  array('numero' => 3, 'prioridad' => 'media', 'descripcion' => 'Tarea de bajada', 'desclarga' =>'Esta es la descripcion larga de la tarea 3'),
          '4' =>  array('numero' => 4, 'prioridad' => 'alta', 'descripcion' => 'Tarea final', 'desclarga' =>'Esta es la descripcion larga de la tarea 4'));

private $tareasdb;

    public function carrega_tasks()
    {
         $em = $this->container->get('doctrine')->getManager();
         $repository = $em->getRepository('FelipeBundle:Tarea');
         $this->tareasdb = $repository->findAll();


    }


    public function taskAction()
    {
        
        return $this->render('FelipeBundle:Default:task.html.twig', array('tasks' => $this->tasks));
    }


    public function detalletaskAction($slug)
    {

        return $this->render('FelipeBundle:Default:detalletask.html.twig', array('task' => $this->tasks[$slug]));
    }


    public function taskdbAction()
    {
        $this->carrega_tasks();
        return $this->render('FelipeBundle:Default:tareasdb.html.twig', array('tareas' => $this->tareasdb));
    }

    public function detalletaskdbAction($slug)
    {
        $em = $this->get('doctrine')->getManager();

        $repository = $em->getRepository('FelipeBundle:Tarea');

        $tarea = $repository->findOneById($slug);

        return $this->render('FelipeBundle:Default:detalletaskdb.html.twig', array('tarea' => $tarea));
    }


    public function creatareaAction(Request $request)
    {
        // crea una tarea y le asigna algunos datos ficticios para este ejemplo
        $tarea = new Tarea();
        //$tarea->setNombre('Escribir en un Blog');
        //$tarea->setData(new \DateTime('tomorrow'));

        $form = $this->createFormBuilder($tarea)
            ->add('nombre', 'text')
            ->add('data', 'date')
            ->add('Gravar', 'submit')
            ->getForm();

        return $this->render('FelipeBundle:Default:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }


}