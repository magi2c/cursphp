<?php

namespace Curso\FelipeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ExampleTemplateController extends Controller
{
    public function taskAction()
    {
         $tasks = array(
            array('numero' => 1, 'prioridad' => 'alta', 'descripcion' => 'Tarea de almacenamiento'),
            array('numero' => 2, 'prioridad' => 'baja', 'descripcion' => 'Tarea de subida'),
            array('numero' => 3, 'prioridad' => 'media', 'descripcion' => 'Tarea de bajada'),
            array('numero' => 4, 'prioridad' => 'alta', 'descripcion' => 'Tarea final'),
        return $this->render('FelipeBundle:Default:task.html.twig', array('tasks' => $tasks));
    }




}

