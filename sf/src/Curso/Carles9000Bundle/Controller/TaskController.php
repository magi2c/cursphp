<?php

namespace Curso\Carles9000Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TaskController extends Controller {

    private $aTaula = array( array( 'nom' => 'Tasca 1', 'tasca' => 'Fer la Tasca 1' ),
                            array(  'nom' => 'Tasca 2', 'tasca' => 'Fer la Tasca 2' ),
                            array(  'nom' => 'Tasca 3', 'tasca' => 'Fer la Tasca 3' )
                            );



    public function indexAction()
    {
//        return $this->render('CursoCarles9000Bundle:task:index.html.twig', array('name' => 'Index Task'));
        return $this->render('CursoCarles9000Bundle:task:index.html.twig', array( 'aTaula' => $this->aTaula ) );

    }

    public function viewAction( $id )
    {
        return $this->render('CursoCarles9000Bundle:task:view.html.twig', array('id' => $id, 'reg' => $this->aTaula[$id - 1] ));
    }    


}

?> 


