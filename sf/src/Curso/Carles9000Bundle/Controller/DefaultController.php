<?php

namespace Curso\Carles9000Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('CursoCarles9000Bundle:Default:index.html.twig', array('name' => $name));
    }

    public function byeAction($name)
    {
        return $this->render('CursoCarles9000Bundle:Default:index.html.twig', array('name' => $name));
    }    

    public function taskAction($name)
    {
        return $this->render('CursoCarles9000Bundle:Default:index.html.twig', array('name' => $name));

    }



}
