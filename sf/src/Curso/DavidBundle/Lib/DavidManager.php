<?php
namespace Curso\DavidBundle\Lib;

use Doctrine\ORM\EntityManager;

class DavidManager{
    protected $mailer;
    protected $params;
    protected $em;

    public function __construct(\Swift_Mailer $mailer, EntityManager $em, $params){
        $this->em = $em;
        $this->mailer = $mailer;
        $this->params = $params;
    }

    public function persistTask(\Curso\DavidBundle\Entity\Task $task){

            $this->em->persist($task);
            $this->em->flush();

//            $contenido = $this->container->get('twig')->render(
//                'CursoDavidBundle:Task:emailnewtask.html.twig',
//                array(  'user'=>'david',
//                        'name'=>$task->getName(),
//                        'date'=>$task->getDate()->format('Y-m-d H:i:s') )
//            );
//
            $contenido = "Test";

            $message = \Swift_Message::newInstance()
                ->setSubject('New task')
                ->setFrom(array('phpurv@gmail.com' => 'Task list manager'))
                ->setTo('david.amoros@gmail.com')
                ->setBody($contenido)
            ;

            $this->mailer->send($message);
    }
}