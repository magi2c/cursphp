<?php
/**
 * Created by PhpStorm.
 * User: public
 * Date: 09/10/2014
 * Time: 12:21
 */

namespace Curso\DavidBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="david_user")
 * @ORM\Entity()
 */

class User {

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=false)
     * @assert\notBlank
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=false)
     * @assert\notBlank
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity="Task", mappedBy="user")
     * solo si no es por defecto:
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $tasks;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getTasks()
    {
        return $this->tasks;
    }
} 