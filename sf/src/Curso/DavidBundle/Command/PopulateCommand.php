<?php
/**
 * Created by PhpStorm.
 * User: public
 * Date: 09/10/2014
 * Time: 13:39
 */

namespace Curso\DavidBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Curso\DavidBundle\Entity\Task;
use Curso\DavidBundle\Entity\User;
use Curso\DavidBundle\Entity\Tag;


class PopulateCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('curso:populate')
            ->setDescription('Populates the database')
            ->addArgument('num', InputArgument::REQUIRED, 'Numero de objetos a crear')
            ->addArgument('type', InputArgument::REQUIRED, 'Tipo de objetos a crear')
            ->setHelp(<<<EOF
The <info>%command.name%</info> :

<info>php %command.full_name%</info>

Inserta task en la bd


<info>php %command.full_name%</info> 10
EOF
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $numElem = $input->getArgument('num');
        $typeElem = $input ->getArgument('type');

        $output->writeln('Creamos <comment>'.$numElem.'</comment> elementos del tipo '.$typeElem);

        $em = $this->getContainer()->get('doctrine')->getManager();

        switch ($typeElem){
            case 'User':
                $repository = $em->getRepository('CursoDavidBundle:User');
                for($i=0;$i<$numElem;$i++){
                    $name = 'usuario'.$i;
                    if(!$repository->findOneByName($name)){
                        $user = new User();
                        $user->setId($i);
                        $user->setName($name);
                        $em->persist($user);

                        $output->writeln('Created user '.$name);
                    }
                    else{
                        $output->writeln('User '.$name.' already exists.');
                    }
                }
                $em->flush();
                break;
            case 'Task':
                $repository = $em->getRepository('CursoDavidBundle:Task');
                for($i=0;$i<$numElem;$i++){
                    $name = 'task'+$i;
                    if(!$repository->findOneByName($name)){
                        $task = new Task();
                        $task->setName($name);
                        $task->setStatus('pending');
                        $date = new \DateTime();
                        $task->setDate($date);

                        $repository2 = $em->getRepository('CursoDavidBundle:Tag');
                        $tagname = 'tag 1';
                        $tag = $repository2->findOneByName($tagname);
                        if(!$tag){
                            $tag = new Tag();
                            $tag->setName($tagname);
                            $em->persist($tag);
                        }
                        $task->addTag($tag);

                        $em->persist($task);
                        $em->flush();

                        $output->writeln('Created task '.$name);
                    }
                    else{
                        $output->writeln('Task '.$name.' already exists.');
                    }
                }
                break;
            case 'Tag':
                $repository = $em->getRepository('CursoDavidBundle:Tag');
                for($i=0;$i<$numElem;$i++){
                    $name = 'tag '+$i;
                    if(!$repository->findOneByName($name)){
                        $tag = new Tag();
                        $tag->setName($name);
                        $em->persist($tag);

                        $output->writeln('Created tag '.$name);
                    }
                    else{
                        $output->writeln('Tag '.$name.' already exists.');
                    }
                }
                $em->flush();
                break;
            default:
                $output->writeln('No se puede instanciar la clase '.$typeElem);
                break;
        }

    }
}
