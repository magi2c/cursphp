<?php

namespace Curso\DavidBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Curso\DavidBundle\Entity\Task;
use Symfony\Component\HttpFoundation\Request;

class TaskController extends Controller
{

    public function indexAction()
    {
        $em = $this->container->get('doctrine')->getManager();
        $repository = $em->getRepository('CursoDavidBundle:Task');
        $tasks = $repository->findAll();

        return $this->render('CursoDavidBundle:Task:index.html.twig',
            array('tasks' => $tasks));
    }

    public function viewAction($id)
    {
        $em = $this->container->get('doctrine')->getManager();
        $repository = $em->getRepository('CursoDavidBundle:Task');
        $task = $repository->find($id);

        return $this->render('CursoDavidBundle:Task:view.html.twig',
            array('task' => $task));
    }

    public function addAction(Request $request)
    {

        $task = new Task();

        $form = $this->createFormBuilder($task)
            ->add('name', 'text')
            ->add('date', 'datetime')
            ->add('status', 'text')
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $this->get('david.services')->persistTask($task);

            $message = $this->get('translator')->trans('Task with ID %id% created successfully',array($task->getId()));

            $this->get('session')->getFlashBag()->add(
                'success',
                $message
            );

            return $this->redirect($this->generateUrl('task_new_success'));
        }

        return $this->render('CursoDavidBundle:Task:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->container->get('doctrine')->getManager();
        $repository = $em->getRepository('CursoDavidBundle:Task');
        $task = $repository->find($id);

        if (!$task) {

            $message =  $this->get('translator')->trans('Task could not be deleted');
            $this->get('session')->getFlashBag()->add(
                'danger',
                $message
            );
        } else {
            $em->remove($task);
            $em->flush();

            $message = $this->get('translator')->trans('Task with ID %id% deleted successfully',$task->getId());
            $this->get('session')->getFlashBag()->add(
                'info',
                $message
            );
        }

        return $this->redirect($this->generateUrl('tasks'));
    }

    public function new_successAction()
    {
        return $this->render('CursoDavidBundle:Task:index.html.twig');
    }

    public function ajaxAddAction(Request $request)
    {

        if ($request->isXmlHttpRequest()) {

            $task = new Task();

            $form = $this->createFormBuilder($task)
                ->add('name', 'text')
                ->add('date', 'datetime')
                ->add('status', 'text')
                ->add('save', 'submit')
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {

                $em = $this->container->get('doctrine')->getManager();

                $em->persist($task);
                $em->flush();

                return new Response('OK');
            }

            return new Response('FAIL',404);
        }
        return ('not allowed');
    }
}