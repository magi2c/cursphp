<?php

namespace Curso\DavidBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name="hola")
    {
        return $this->render('CursoDavidBundle:Default:index.html.twig', array('name' => $name));
    }

    public function miAccionAction($accion)
    {
        switch($accion)
        {
            case 'hola': return $this->render('CursoDavidBundle:Default:index.html.twig', array('name' => $accion));
            default : return $this->render('CursoDavidBundle:Default:accion.html.twig',array('accion'=> $accion));
        }
    }
}
