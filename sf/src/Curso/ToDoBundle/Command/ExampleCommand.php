<?php

namespace Curso\ToDoBundle\Command;

use Curso\ToDoBundle\Entity\Task;
use Curso\ToDoBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class ExampleCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('curso:example')
            ->setDescription('Create tasks')
            ->addArgument('num', InputArgument::REQUIRED, 'Numero de taskcas creadas')
            ->addOption('sufix', 'u', InputOption::VALUE_OPTIONAL, 'Sufix task')
            ->setHelp(<<<EOF
The <info>%command.name%</info> :

<info>php %command.full_name%</info>

Inserta task en la bd


<info>php %command.full_name%</info> 10
EOF
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $numTask = $input->getArgument('num');

        $em = $this->getContainer()->get('doctrine')->getManager();


        $output->writeln('Creamos <comment>'.$numTask.'</comment>!...'.$input->getOption('sufix'));

        $user = new User();
        $user->setEmail('nanan@gmail.com');
        $user->setNombre('nananan');
        $em->persist($user);

        $task = new Task();
        $task->setNombre('test');
        $task->setUser($user);
        $task->setData(new \DateTime());
        $em->persist($task);

        $em->flush();






    }
}
