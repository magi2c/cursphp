<?php

namespace Curso\ToDoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ExampleControllerController extends Controller
{
    public function indexAction()
    {
        return $this->render('CursoToDoBundle:ExampleController:index.html.twig', array('name' => 'Ejemplo Controller'));
    }

    public function renderAction()
    {
        $content = $this->renderView(
            'CursoToDoBundle:ExampleController:index.html.twig',
            array('name' => 'Ejemplo Controller (renderAction)')
        );


        return new Response($content);
    }


    public function redirectAction()
    {
      return $this->redirect($this->generateUrl('example_controller'));
    }


    public function redirect301Action()
    {
        return $this->redirect($this->generateUrl('example_controller'), 301);
    }


    public function forwardAction()
    {
        $response = $this->forward('CursoToDoBundle:ExampleController:index', array());

        return $response;
    }


    public function requestAction(Request $request)
    {
        print_r($_GET);
        $post = $request->request->all();
        print_r($post);

        $get = $request->query->all();
        print_r($get);

        die();
    }

    public function extendsAction()
    {
        //ToDo
        $mailer = $this->container->get('mailer');

        if (false) {
            throw $this->createNotFoundException('x no existe.');

            //throw new \Exception('ooo');
        }

        return $this->render('CursoToDoBundle:ExampleController:index.html.twig', array('name' => 'Hola Curso'));
    }

    /**
     * Gestionando la sesión
     * Por defecto Symfony2 almacena la información en una cookie usando las sesiones nativas de PHP.
     *
     * @return Response
     */
    public function sessionAction()
    {
        $request = $this->get('request');
        $session = $request->getSession();

        $cargas = $session->get('cargas', 0);
        $cargas ++;
        $session->set('cargas', $cargas);

        //Almacenar que solo está disponible durante la siguiente petición
        $this->get('session')->getFlashBag()->add(
            'notice',
            'Se han guardado los cambios.'
        );
        //die('demo flash');

        return $this->render('CursoToDoBundle:ExampleController:session.html.twig', array('cargas' => $cargas));
    }


}
