<?php

namespace Curso\ToDoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ExampleBootstrapController extends Controller
{
    public function layoutAction()
    {
        return $this->render('CursoToDoBundle:ExampleBootstrap:include_layout.html.twig');
    }

    public function coverAction()
    {
        return $this->render('CursoToDoBundle:ExampleBootstrap:cover.html.twig');
    }

    public function basicAction()
    {
        return $this->render('CursoToDoBundle:ExampleBootstrap:basic.html.twig');
    }

    public function completedAction()
    {
        return $this->render('CursoToDoBundle:ExampleBootstrap:completed.html.twig');
    }

    public function treeviewAction()
    {
        return $this->render('CursoToDoBundle:ExampleBootstrap:treeview.html.twig');
    }

    public function formsAction()
    {
        return $this->render('CursoToDoBundle:ExampleBootstrap:forms.html.twig');
    }
}
