<?php

namespace Curso\ToDoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class ExampleJqueryController extends Controller
{
    public function example1Action(Request $request)
    {
        return $this->render('CursoToDoBundle:ExampleJQuery:example1.html.twig');
    }

    public function example2Action(Request $request)
    {
        return $this->render('CursoToDoBundle:ExampleJQuery:example2.html.twig');
    }

    public function example3Action(Request $request)
    {
        return $this->render('CursoToDoBundle:ExampleJQuery:example3.html.twig');
    }

    public function example4Action(Request $request)
    {
        return $this->render('CursoToDoBundle:ExampleJQuery:example4.html.twig');
    }

    public function example5Action(Request $request)
    {
        return $this->render('CursoToDoBundle:ExampleJQuery:example5.html.twig');
    }

    public function example6Action(Request $request)
    {
        return $this->render('CursoToDoBundle:ExampleJQuery:example6.html.twig');
    }

    public function example7Action(Request $request)
    {
        return $this->render('CursoToDoBundle:ExampleJQuery:example7.html.twig');
    }

    public function example8Action(Request $request)
    {
        return $this->render('CursoToDoBundle:ExampleJQuery:example8.html.twig', array('hora' => date("h:i:sa")));
    }

    public function example9Action(Request $request)
    {
        return $this->render('CursoToDoBundle:ExampleJQuery:example9.html.twig', array('hora' => date("h:i:sa")));
    }

    public function example10Action(Request $request)
    {
        return $this->render('CursoToDoBundle:ExampleJQuery:example10.html.twig');
    }

    public function ajax1Action(Request $request)
    {
        echo date("h:i:sa");
        die();
    }

    public function ajax2Action(Request $request)
    {
        if ($request->isXmlHttpRequest()) {

            $ciudad = $request->get('ciudad');
            switch ($ciudad) {
                case 'tarragona':
                    $time = date("h:i:sa");
                    break;
                case 'canarias':
                    $time = date("h:i:sa", strtotime('-1 hour'));
                    break;
                default:
                    return new Response("No tenemos esta ciudad", 404);
                    break;
            }
            return new Response($time);
        }
        die();
    }


    public function ajax3Action(Request $request)
    {

        if ($request->isXmlHttpRequest()) {

            $time_tarragona = date("h:i:sa");
            $time_canarias =  date("h:i:sa", strtotime('-1 hour'));
            $ciudades = Array('tarragona' => array('ciudad' => 'Tarragona',
                'date'   => array('hour'    => date("h"),
                    'minute'  => date("i"),
                    'second' => date("s"))
            ),
                'canarias' => array('ciudad' => 'Gran Canaria',
                    'date'   => array('hour'    => date("h", strtotime('-1 hour')),
                        'minute'  => date("i", strtotime('-1 hour')),
                        'second' => date("s", strtotime('-1 hour')))
                )
            );
            $response = new Response(json_encode($ciudades));
            $response->headers->set('Content-Type', 'application/json');

            return $response;
        }
        die();
    }
}
