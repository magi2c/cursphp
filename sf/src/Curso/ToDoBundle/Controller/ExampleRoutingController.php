<?php

namespace Curso\ToDoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ExampleRoutingController extends Controller
{
    /**
     * Ruta básica
     */
    public function indexAction()
    {
       die('routing');
    }

    /**
     * Rutas con variables
     *
     * @param $slug
     */
    public function variableAction($slug)
    {
        die('routing + '.$slug);
    }

    /**
     * Variables obligatorias y opcionales
     *
     * @param $slug
     */
    public function variableOpAction($slug)
    {
        die('routing + '.$slug);
    }

    /**
     * Variables con requisitos
     *
     * @param $slug
     * @param $_locale
     */
    public function variableReqAction($slug, $_locale)
    {
        die('routing + '.$slug.' -> '.$_locale);
    }

    /**
     * Variables con requisitos HTTP
     */
    public function variableReqHttpAction()
    {
        die('routing');
    }


    /**
     * Generando URL
     */
    public function serviceAction()
    {
        $router = $this->container->get('router');

        $params = $router->match('/curso/example/routing/variable_req/3/es.html');

        echo '<pre>'; print_r($params);

        $uri = $router->generate('example_routing_requisitos',
            array('slug' => '3', '_locale' => 'ca', '_format' => 'html'));


        echo "\n\n\n".$uri."\n\n\n";

        //Generando URL absolutas
        $url = $router->generate('example_routing_requisitos',
            array('slug' => '3', '_locale' => 'es', '_format' => 'html'), true);


        die($url);

    }

}
