<?php

namespace Curso\ToDoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ExampleTemplateController extends Controller
{
    public function indexAction()
    {
        return $this->render('CursoToDoBundle:ExampleTemplate:index.html.twig', array('name' => 'ejemplo template'));
    }


    public function herenciaAction()
    {
        $users = array(
            array('nombre' => 'pepe', 'validado' => true),
            array('nombre' => 'Marc', 'validado' => true));

        return $this->render('CursoToDoBundle:ExampleTemplate:herencia.html.twig', array('users' => $users));
    }




}
