<?php

namespace Curso\ToDoBundle\Controller;

use Curso\ToDoBundle\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ExampleTransController extends Controller
{
    public function indexAction()
    {
        $ofertaDelDia = $this->get('translator')->trans('Oferta del día');

        ladybug_dump_die($ofertaDelDia, $this->get('translator'));
        //ladybug_dump($this->getRequest())
        return $this->render('CursoToDoBundle:ExampleTrans:index.html.twig', array('ofertaDelDia' => $ofertaDelDia));
    }

    public function indexManagerAction()
    {
        $this->get('curso_to_do.example')->persistTask(new Task());

        ladybug_dump_die('manager');
    }

    public function indexPdfAction()
    {


//        $ofertaDelDia = $this->get('translator')->trans('Oferta del día');
//
//        $pdfGenerator = $this->get('siphoc.pdf.generator');
//        $pdfGenerator->setName('my_pdf.pdf');
//        return $pdfGenerator->downloadFromView(
//            'CursoToDoBundle:ExampleTrans:index.html.twig', array(
//                'ofertaDelDia' => $ofertaDelDia
//            )
//        );
    }






}
