<?php

namespace Curso\ToDoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Curso\ToDoBundle\Entity\Todo;

class ExampleFormsController extends Controller
{
    public function newAction(Request $request)
    {
        // crea una task y le asigna algunos datos ficticios para este ejemplo
        $task = new Todo();
        $form = $this->createFormBuilder($task)
            ->add('nombre', 'text')
            ->add('user', 'text')
            ->add('save', 'submit')
            ->getForm();

        return $this->render('CursoToDoBundle:ExampleForms:forms.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
