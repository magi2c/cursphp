<?php

namespace Curso\TxominBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Task
 *
 * @ORM\Table(name="txomin_task")
 * @ORM\Entity(repositoryClass="Curso\TxominBundle\Entity\TaskRepository")
 */
class Task
{

    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     *
     * @ORM\Column(name="data", type="datetime")
     */
    private $data;

    /**
     * @ORM\ManyToMany(targetEntity="Tag")
     * @ORM\JoinTable(name="txomin_task_tags",
     *      joinColumns={@ORM\JoinColumn(name="task_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
     *      )
     */
     private $tags;


    /**
     * @ORM\ManyToOne(targetEntity="User")
     * solo si no es por defecto:
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     * @Assert\NotBlank
     */
    private $user;


    public function __construct() {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getData()
    {

        return $this->data;
    }

    /**
     * @param string $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }


    public function getTagsFormatted()
    {
        $names=array();
        foreach($this->tags as $tag){
               $names[]=$tag->getNombre();
        }
        return implode (",",$names);
    }

    /**
     * @param mixed $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    public function addTag($tag)
    {
        $this->tags->add($tag);
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }


    public function __toString()
    {
        return "ID:\t\t".$this->getId()."\nNombre:\t\t".$this->getNombre()."\nTags:\t\t".$this->getTagsFormatted()."\nUsuari: \t[".$this->getUser()->getId()."] ".$this->getUser()->getNombre()."\n";
    }






}

