<?php

namespace Curso\TxominBundle\Command;

use Curso\TxominBundle\Entity\Tag;
use Curso\TxominBundle\Entity\Task;
use Curso\TxominBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;



class SearchDBCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('txomin:searchdb')
            ->setDescription('Search entities from database')
            ->addArgument('entity', InputArgument::REQUIRED, 'Entidad a buscar')
            ->addArgument('id', InputArgument::REQUIRED, 'Id de la entidad')
            ->setHelp(<<<EOF
The <info>%command.name%</info> :

<info>php %command.full_name%</info>

<info>php %command.full_name%</info> 10
EOF
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $contenedor = $this->getContainer();
        $em = $contenedor->get('doctrine')->getManager();


        $entidad= $input->getArgument('entity');
        $id= $input->getArgument('id');



        switch($entidad){
            case "user":  $repo=$em->getRepository('CursoTxominBundle:User');break;
            case "task":  $repo=$em->getRepository('CursoTxominBundle:Task');break;
            case "tag":  $repo=$em->getRepository('CursoTxominBundle:Tag');break;
            default:break;
        }
        if(isset($repo)) {

            if($id=="all"){
                $output->writeln('Buscamos todas las entidades "'.$entidad.'"');
                $objects = $repo->findAll();
                $output->writeln('Resultados:'.count($objects));
                foreach($objects as $obj){
                    $output->writeln($obj->__toString());
                }

            }else if(is_numeric($id)){

                $output->writeln('Buscamos "'.$entidad.'" con ID '.$id);
                $object = $repo->findOneById($id);

                $output->writeln('Resultado:');
                if ($object != null) {
                    $output->writeln($object->__toString());
                } else {
                    $output->writeln("No results");
                }
            }

        }else{
            $output->writeln("Entity not valid [user,task,tag]");
        }


    }
}
