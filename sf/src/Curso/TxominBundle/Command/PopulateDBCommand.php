<?php

namespace Curso\TxominBundle\Command;

use Curso\TxominBundle\Entity\Tag;
use Curso\TxominBundle\Entity\Task;
use Curso\TxominBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;




class PopulateDBCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('txomin:populatedb')
            ->setDescription('Insert dummy tasks into database')
            ->addArgument('num', InputArgument::REQUIRED, 'Numero de tareas a crear')
            ->setHelp(<<<EOF
The <info>%command.name%</info> :

<info>php %command.full_name%</info>

Inserta tascas en la bd

<info>php %command.full_name%</info> 10
EOF
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $numUsers = $input->getArgument('num');

        $contenedor = $this->getContainer();
        $em = $contenedor->get('doctrine')->getManager();


        $output->writeln('Creamos '.$numUsers.' usuarios...');
        for($i=0;$i<$numUsers;$i++) {
            $output->writeln('');
            $output->writeln('#######################');
            $output->writeln('Creando Usuario #' . ($i + 1));

            $user = new User();
            $dialog = $this->getHelperSet()->get('dialog');
            $nombre = $dialog->ask($output, '<question>Introduce un nombre de usuario: </question>', 'Txomin Medrano');
            $email = $dialog->ask($output, '<question>Introduce un email de usuario: </question>', 'txomin@tarragona.cat');
            $user->setEmail(utf8_encode($email));
            $user->setNombre(utf8_encode($nombre));
            $em->persist($user);


            $numTask = $dialog->askAndValidate($output, '<question>Introduce el numero de tareas a crear para el usuario "' .$nombre . '": </question>',
                function($valor) {
                    if (false == is_numeric($valor)) {
                        throw new \InvalidArgumentException('EP! "'.$valor.'" no es un numero');
                    }
                    return $valor;
                },2);


            for ($j = 0; $j < $numTask; $j++) {

                $output->writeln('');
                $output->writeln('Creando Tarea #' . ($j + 1));
                $output->writeln('---------------------------');

                $task = new Task();
                $nombretarea = $dialog->ask($output, '<question>Introduce el nombre de la tarea #' . ($j+ 1) . ' para el usuario "' .$nombre . ': </question>', 'Tarea 1');

                $task->setNombre(utf8_encode($nombretarea));
                $task->setData(new \DateTime('now'));
                $task->setUser($user);


                $tags = $dialog->ask($output, '<question>Introduce las etiquetas (separadas por comas) para el usuario "' .$nombre . ': </question>', 'etiqueta');
                $etiqs = explode(",", $tags);

                foreach ($etiqs as $etiq) {
                    $tag = new Tag();
                    $tag->setNombre(utf8_encode($etiq));
                    $em->persist($tag);
                    $task->addTag($tag);
                }


                $em->persist($task);

            }
        }
        $em->flush();



    }
}
