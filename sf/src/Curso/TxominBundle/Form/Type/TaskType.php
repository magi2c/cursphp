<?php

namespace Curso\TxominBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TaskType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $t = $options['t'];


        $builder
            ->add('id', 'hidden')
            ->add('nombre', 'text',array('label'=>$t->trans('tasks.name')))
            ->add('data', 'date',array('label'=>$t->trans('tasks.date')))
            ->add('tags', 'entity', array(
                'label'=>$t->trans('tasks.tags'),
                'class' => 'CursoTxominBundle:Tag',
                'property' => 'nombre',
                'multiple' =>true,
                'expanded' => true

            ))
            ->add('user', 'entity', array(
                'label'=> $t->trans('users.user') ,
                'class' => 'CursoTxominBundle:User',
                'property' => 'nombre',
                'empty_value' => 'Choose an option'

            ))
            ->add('save', 'submit',array('label'=>$t->trans('forms.save')));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Curso\TxominBundle\Entity\Task',
            't' => 'Symfony\Bundle\FrameworkBundle\Translation\Translator'
        ));
    }

    public function getName()
    {
      return 'curso_txominbundle_tasktype';
    }
}