<?php

namespace Curso\TxominBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $t = $options['t'];

        $builder
            ->add('id', 'hidden')
            ->add('nombre', 'text' , array('label'=> $t->trans('users.name') ))
            ->add('email', 'email' , array('label'=> $t->trans('users.email') ))
            ->add('save', 'submit', array('label'=> $t->trans('forms.save') ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Curso\TxominBundle\Entity\User',
            't' => 'Symfony\Bundle\FrameworkBundle\Translation\Translator'
        ));
    }

    public function getName()
    {
      return 'curso_txominbundle_usertype';
    }
}