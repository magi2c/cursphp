<?php

namespace Curso\TxominBundle\Lib;

use Curso\TxominBundle\Entity\Task;
use Doctrine\ORM\EntityManager;

class CursoTxominManager
{
    protected $em;
    protected $translator;
    protected $templating;
    protected $mailer;

    public function __construct(EntityManager $em, $translator, $templating, \Swift_Mailer $mailer)
    {
        $this->em     = $em;
        $this->translator = $translator;
        $this->templating = $templating;
        $this->mailer = $mailer;

    }

    public function persistTask(Task $task,$sendmail=true)
    {

        //guardo en BBDD
        $this->em->persist($task);
        $this->em->flush();
        if($sendmail) {
            //envio el mail
            $mailbody = $this->templating->render('CursoTxominBundle:Mail:task.html.twig', array('task' => $task));

            $subject = $this->translator->trans('mail.subject %taskid%', array('%taskid%' => $task->getId()));

            $mensaje = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom(array('mailing@txomintasks.com' => 'TxominTasks'))
                ->setTo('tmedrano@gmail.com')
                ->setBody($mailbody, 'text/html');

            $this->mailer->send($mensaje);
        }

    }

    public function getTask($id){
         $repository = $this->em->getRepository('CursoTxominBundle:Task');
         return $repository->findOneById($id);
    }

    public function removeTask($id){
        $repository = $this->em->getRepository('CursoTxominBundle:Task');
        $task=$repository->findOneById($id);
        $this->em->remove($task);
        $this->em->flush();
    }
}
