$(document).ready(function(){

    bootbox.setDefaults({
        locale: $('html').data('locale')
    });



    $('.btn-danger').click(function(e){
        e.preventDefault();
        var href=$(this).attr('href');

        var button=$(this);

       /* bootbox.confirm("Are you sure?", function(result) {
            if(result){
                window.location = href;
            }
        });
        */
         bootbox.dialog({
             message: button.data('message'),
             title: button.data('title'),
             buttons: {
                 cancel: {
                     label: button.data('cancel'),
                     className: "btn-default",
                     callback: function() {

                     }
                 },
                 success: {
                     label: button.data('ok'),
                     className: "btn-primary",
                     callback: function() {
                         window.location = href;
                     }
                 }
             }
         });

    });






});