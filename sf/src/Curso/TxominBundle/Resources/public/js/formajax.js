$(document).ready(function(){

    $('form').submit(function(e){
        e.preventDefault();


        var f=$(this);

        var params= f.serialize();

        //bloqueo el formulario
        f.find('input').attr('disabled',true);
        f.parent().addClass('loading');
        f.find('.actions button,.actions a.btn').addClass('disabled').attr('disabled',true);
        var oldtext=f.find('button[type="submit"]').html();
        f.find('button[type="submit"]').html('Guardando...');

        var al=$('<div/>');
        al.addClass('alert');
        al.html('<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>');


         $('#progress .progress-bar').attr('aria-valuenow',50).css('width','50').find('.sr-only').html('50%');
         $('#progress').addClass('loading');





        var url=f.find('#ajaxurl').val();

        var ax=$.post(url,params,function(data) {
            //si ok
            al.addClass('alert-success');
            al.append('<span class="glyphicon glyphicon-ok"></span>&nbsp;'+data);
            $('#progress .progress-bar').attr('aria-valuenow',80).css('width','80%').find('.sr-only').html('80%');

        }).fail(function(data) {
           //si falla
            al.addClass('alert-danger');
            console.log(data);
            al.append('<span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;'+data.responseText);
            $('#progress .progress-bar').attr('aria-valuenow',50).css('width','50%').find('.sr-only').html('50%');

        }).always(function() {
            $('.flashbag-container').append(al);
            f.find('input').attr('disabled',false);
            f.parent().removeClass('loading');
            f.find('.actions button,.actions a.btn').removeClass('disabled').attr('disabled',false);
            f.find('button[type="submit"]').html(oldtext);
            $('#progress .progress-bar').attr('aria-valuenow',100).css('width','100%').find('.sr-only').html('Completado');
            $('#progress .progress-bar').removeClass('progress-bar-striped').removeClass('active');
            $('#progress').removeClass('loading');
        });










    });

});