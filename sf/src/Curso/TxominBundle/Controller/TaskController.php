<?php
/**
 * Created by PhpStorm.
 * User: public
 * Date: 09/10/2014
 * Time: 9:14
 */

namespace Curso\TxominBundle\Controller;

use Curso\TxominBundle\Entity\Task;
use Curso\TxominBundle\Form\Type\TaskType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class TaskController extends Controller
{
    private $tasks = array();

    public function listallAction()
    {
        $em=$this->container->get('doctrine')->getManager();
        $repository = $em->getRepository('CursoTxominBundle:Task');
        $this->tasks=$repository->findAll();

        return $this->render('CursoTxominBundle:Task:list.html.twig',array(
                'tasks'=>$this->tasks
        ));
    }
    public function viewAction($id)
    {
        $task=$this->container->get('curso_txomin.manager')->getTask($id);
        //ladybug_dump_die($this->get('translator'));
        return $this->render('CursoTxominBundle:Task:view.html.twig',array(
            'task'=>$task
        ));
    }

    public function newAction(Request $request)
    {
        // crea una task y le asigna algunos datos ficticios para este ejemplo
        $task = new Task();
        $form = $this->createForm(new TaskType(), $task, array('t' => $this->get('translator')));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->container->get('curso_txomin.manager')->persistTask($task);

            $msg = $this->get('translator')->trans('tasks.insert_success %taskid%', array('%taskid%' => $task->getId()) );
            $this->get('session')->getFlashBag()->add( 'success', $msg );
            return $this->redirect($this->generateUrl('curso_txomin_tasks'));
        }
        return $this->render('CursoTxominBundle:Task:form.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function editAction($id,Request $request)
    {
        $task=$this->container->get('curso_txomin.manager')->getTask($id);
        $form = $this->createForm(new TaskType(), $task, array('t' => $this->get('translator')));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->container->get('curso_txomin.manager')->persistTask($task,false);

            $msg = $this->get('translator')->trans('tasks.update_success %taskid%', array('%taskid%' => $task->getId()));
            $this->get('session')->getFlashBag()->add('success',$msg);

        }
        return $this->render('CursoTxominBundle:Task:form.html.twig', array('form' => $form->createView(),'id'=>$task->getId()));

    }

    public function removeAction($id)
    {
        $this->container->get('curso_txomin.manager')->removeTask($id);

        $msg = $this->get('translator')->trans('tasks.delete_success %taskid%',array('%taskid%' => $id));
        $this->get('session')->getFlashBag()->add('success',$msg);
        return $this->redirect($this->generateUrl('curso_txomin_tasks'));
    }

    public function ajaxnewAction(Request $request){
        if ($request->isXmlHttpRequest()) {
            $task = new Task();
            $form = $this->createForm(new TaskType(), $task, array('t' => $this->get('translator')));
            $form->handleRequest($request);

            //var_dump($request->attributes);
            if ($form->isSubmitted()){
                if ($form->isValid()) {
                    // guardar la tarea en la base de datos

                    $this->container->get('curso_txomin.manager')->persistTask($task);

                    $msg = $this->get('translator')->trans('tasks.insert_success %taskid%', array('%taskid%' =>  $task->getId()));
                    $response = new Response($msg);

                } else {
                    $errors = (string) $form->getErrors(true, false);
                    $response = new Response($errors, 500 );
                }
                return $response;
            }


        }
        die();
    }

    public function ajaxeditAction(Request $request){
        if ($request->isXmlHttpRequest()) {

            $id=$request->get('curso_txominbundle_tasktype')['id'];
            $task=$this->container->get('curso_txomin.manager')->getTask($id);


            $form = $this->createForm(new TaskType(), $task, array('t' => $this->get('translator')));

            $form->handleRequest($request);

            //var_dump($request->attributes);
            if ($form->isSubmitted()){
                if ($form->isValid()){
                    // guardar la tarea en la base de datos
                    $this->container->get('curso_txomin.manager')->persistTask($task,false);

                    $msg = $this->get('translator')->trans('tasks.update_success %taskid%',array('%taskid%' =>  $task->getId()));
                    $response = new Response($msg);

                } else {
                    $errors = (string) $form->getErrors(true, false);
                    $response = new Response($errors, 500 );
                }
                return $response;
            }


        }
        die();
    }


}