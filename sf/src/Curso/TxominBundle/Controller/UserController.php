<?php
/**
 * Created by PhpStorm.
 * User: public
 * Date: 09/10/2014
 * Time: 9:14
 */

namespace Curso\TxominBundle\Controller;

use Curso\TxominBundle\Entity\User;
use Curso\TxominBundle\Form\Type\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class UserController extends Controller
{

    public function listallAction()
    {
        $em=$this->container->get('doctrine')->getManager();
        $repository = $em->getRepository('CursoTxominBundle:User');
        $users=$repository->findAll();

        return $this->render('CursoTxominBundle:User:list.html.twig',array(
                'users'=>$users
        ));
    }
    public function viewAction($id)
    {
        $em=$this->container->get('doctrine')->getManager();
        $repository = $em->getRepository('CursoTxominBundle:User');
        $user=$repository->findOneById($id);

        return $this->render('CursoTxominBundle:User:view.html.twig',array(
            'user'=>$user
        ));
    }



    private function getErrorMessages(Form $form) {
        $errors = array();

        if ($form->hasChildren()) {
            foreach ($form->getChildren() as $child) {
                if (!$child->isValid()) {
                    $errors[$child->getName()] = $this->getErrorMessages($child);
                }
            }
        } else {
            foreach ($form->getErrors() as $key => $error) {
                $errors[] = $error->getMessage();
            }
        }

        return $errors;
    }



    public function newAction(Request $request)
    {
        $user = new User();

        $form = $this->createForm(new UserType(), $user, array('t' => $this->get('translator')));

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                // guardar la tarea en la base de datos
                $em = $this->container->get('doctrine')->getManager();

                $em->persist($user);
                $em->flush();

                $msg = $this->get('translator')->trans(
                    'users.insert_success %userid%',
                    array('%userid%' =>  $user->getId())
                );

                $this->get('session')->getFlashBag()->add(
                    'success',
                    $msg
                );
                return $this->redirect($this->generateUrl('curso_txomin_users'));


            } else {
                $errors=$form->getErrors();

                $msg = $this->get('translator')->trans(
                    'users.insert_error %useremail%',
                    array('%useremail%' =>  $user->getEmail())
                );

                $this->get('session')->getFlashBag()->add(
                    'danger',
                    $msg
                );
            }
        }
        return $this->render('CursoTxominBundle:User:form.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function editAction($id,Request $request)
    {
        $em=$this->container->get('doctrine')->getManager();
        $repository = $em->getRepository('CursoTxominBundle:User');
        $user=$repository->findOneById($id);

        $form = $this->createForm(new UserType(), $user, array('t' => $this->get('translator')));

        $form->handleRequest($request);

        if ($form->isValid()) {
            // guardar la tarea en la base de datos


            $em->persist($user);
            $em->flush();

            $msg = $this->get('translator')->trans(
                'users.update_success %userid%',
                array('%userid%' =>  $user->getId())
            );

            $this->get('session')->getFlashBag()->add(
                'success',
               $msg
            );

        }


        return $this->render('CursoTxominBundle:User:form.html.twig', array(
            'form' => $form->createView(),
            'id'=>$user->getId()
        ));

    }

    public function removeAction($id)
    {
        $em=$this->container->get('doctrine')->getManager();
        $repository = $em->getRepository('CursoTxominBundle:User');
        $user=$repository->findOneById($id);

        $em->remove($user);
        $em->flush();

        $msg = $this->get('translator')->trans(
            'users.delete_success %userid%',
            array('%userid%' =>  $id)
        );

        $this->get('session')->getFlashBag()->add(
            'success',
            $msg
        );
        return $this->redirect($this->generateUrl('curso_txomin_users'));
    }


    public function ajaxnewAction(Request $request){
        if ($request->isXmlHttpRequest()) {
            $user = new User();
            $form = $this->createForm(new UserType(), $user, array('t' => $this->get('translator')));
            $form->handleRequest($request);

            //var_dump($request->attributes);
            if ($form->isSubmitted()){
                if ($form->isValid()) {
                    // guardar la tarea en la base de datos
                    $em = $this->container->get('doctrine')->getManager();
                    $em->persist($user);
                    $em->flush();

                    $msg = $this->get('translator')->trans(
                        'users.insert_success %userid%',
                        array('%userid%' =>  $user->getId())
                    );
                    $response = new Response($msg);

                } else {
                    $msg = $this->get('translator')->trans(
                        'users.insert_error %useremail%',
                        array('%useremail%' =>  $user->getEmail())
                    );
                    $response = new Response($msg, 500 );
                }
                return $response;
            }


        }
        die();
    }

    public function ajaxeditAction(Request $request){
        if ($request->isXmlHttpRequest()) {
            $em=$this->container->get('doctrine')->getManager();
            $repository = $em->getRepository('CursoTxominBundle:User');

            $id=$request->get('curso_txominbundle_usertype')['id'];


            $user=$repository->findOneById($id);

            $form = $this->createForm(new UserType(), $user, array('t' => $this->get('translator')));

            $form->handleRequest($request);

            //var_dump($request->attributes);
            if ($form->isSubmitted()){
                if ($form->isValid()){
                    // guardar la tarea en la base de datos
                    $em->persist($user);
                    $em->flush();

                    $msg = $this->get('translator')->trans(
                        'users.update_success %userid%',
                        array('%userid%' =>  $user->getId())
                    );
                    $response = new Response($msg);

                } else {
                    $errors = (string) $form->getErrors(true, false);
                    $response = new Response($errors, 500 );
                }
                return $response;
            }


        }
        die();
    }

}