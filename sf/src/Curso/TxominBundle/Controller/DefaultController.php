<?php

namespace Curso\TxominBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('CursoTxominBundle:Default:index.html.twig', array('name' => $name));
    }


    public function operaAction($operacion,$op1,$op2)
    {
        $resultado=0;
        $simbolo="";
        switch($operacion){
            case "suma": $simbolo="+";$resultado=$op1+$op2;break;
            case "resta": $simbolo="-";$resultado=$op1-$op2;break;
            case "multiplica": $simbolo="*";$resultado=$op1*$op2;break;
        }

        return $this->render('CursoTxominBundle:Default:result.html.twig',
            array(
                'simbolo' => $simbolo,
                'resultado' => $resultado,
                'op1' => $op1,
                'op2' => $op2

            )
        );
    }
}
