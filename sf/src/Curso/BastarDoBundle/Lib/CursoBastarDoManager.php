<?php
namespace Curso\BastarDoBundle\Lib;

use Curso\BastarDoBundle\Entity\User as User;
use Curso\BastarDoBundle\Entity\Task as Task;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class CursoBastarDoManager
{
    protected $user;
    protected $task;
    protected $em;
    protected $cont;

    public function __construct(EntityManager $em, Container $cont)
    {
        $this->em = $em;
//        $this->user = $u;
//        $this->task = $t;
        $this->cont = $cont;
    }

    public function persistUser($nameuser, $numTasks)
    {
        $generator = $this->cont->get('security.secure_random');

        $this->user = new User();
        $this->user->setName($nameuser);

        try {
            for ($i = 0; $i < $numTasks; $i++) {
                $this->task = new Task();
                $this->task->setTime(new \DateTime());

                // ramdom number 8bytes
                $bytes = $generator->nextBytes(8);
                $hexEncoded = bin2hex($bytes);
                $this->task->setState("Active" . $hexEncoded);
                $this->em->persist($this->task);
                $this->user->addTask($this->task);
            }

            $this->em->persist($this->user);
            $this->em->flush();
        } catch (\Exception $e) {
            throw new \Exception($e);
        }

    }
}
