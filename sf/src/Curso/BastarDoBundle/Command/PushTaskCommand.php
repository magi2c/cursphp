<?php

namespace Curso\BastarDoBundle\Command;

use Curso\BastarDoBundle\Entity\Task;
use Curso\BastarDoBundle\Entity\User;
use DateTime;
//use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PushTaskCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('bastardo:createtasks')
            ->setDescription('Crear tareas')
            ->addArgument('name', InputArgument::REQUIRED, 'Nombre del USUARIO')
            ->addOption('tasks', 'u', InputOption::VALUE_OPTIONAL, 'Num. de TAREAS')
            ->setHelp(<<<EOF
The <info>%command.name%</info> :

<info>php %command.full_name%</info>

Inserta usuarios/tareas en la BD


<info>php %command.full_name%</info> 10
EOF
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $nameUser = utf8_encode($input->getArgument('name'));
        $numTask = $input->getOption('tasks');

        // global container ...
        $cont = $this->getContainer();
        $em = $cont->get('doctrine')->getManager();
        $rep = $em->getRepository('CursoBastarDoBundle:User');
        $generator = $cont->get('security.secure_random');

        // Ckeck if user exists...
        $user = $rep->findOneByName($nameUser);
        //var_dump($user);
        if( !$user ) {
            $user = new User();
            $user->setName($nameUser);

           // $t = new ArrayCollection();
            for ($i = 0; $i < $numTask; $i++) {
                $task = new Task();
                $task->setTime(new DateTime());

                // ramdom number 8bytes
                $bytes = $generator->nextBytes(8);
                $hexEncoded = bin2hex($bytes);

                $task->setState("Active" . $hexEncoded);
                $em->persist($task);
              //  $t->add($task);
                $user->addTask($task);
            }
            // $user->setTasks($t);
            $em->persist($user);
            $output->writeln('Creamos USUARIO <comment>'.$nameUser.'</comment> amb '.$numTask. ' TAREAS');
        }
        else {
            $output->writeln( 'USUARIO '. $nameUser . ' ja existeix!!!');
        }

    // final
        $em->flush();
    }
}
