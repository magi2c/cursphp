<?php

namespace Curso\BastarDoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Curso\BastarDoBundle\Entity\Task;
use Curso\BastarDoBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\HttpFoundation\Response;

class TaskController extends Controller
{
    protected $user;
    protected $listUsers;

    public function indexAction()
    {
        // $title = $this->get('translator')->trans('list.users');
        $this->listUsers = $this->getDoctrine()->getRepository('CursoBastarDoBundle:User')->findAll();
        return $this->render('CursoBastarDoBundle:Default:index.html.twig', array('users' => $this->listUsers));
    }

    public function viewAction($id)
    {
        $this->user = $this->getDoctrine()->getRepository('CursoBastarDoBundle:User')->find($id);

        if (!$this->user) {
            $this->user = new User();
        }

        return $this->render('CursoBastarDoBundle:Default:view.html.twig',
            array('username' => $this->user->getName(), 'tasks' => $this->user->getTasks())
        );
    }

    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $generator = $this->container->get('security.secure_random');

        $user = new User();
        $form = $this->createFormBuilder($user)
            ->add('name', 'text', array('label' => 'Name', 'attr' => array('class' => 'control-label')))
            ->add('tasks', 'text', array('constraints' => array(new NotBlank()), 'mapped' => false, 'attr' => array('class' => 'control-label')))
            ->add('save', 'submit', array('label' => 'New user', 'attr' => array('class' => 'btn btn-default')))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            // guardar la tarea en la base de datos
            $numTasks = $form->get('tasks')->getData();

            for ($i = 0; $i < $numTasks; $i++) {
                $task = new Task();
                $task->setTime(new \DateTime());

                // ramdom number 8bytes
                $bytes = $generator->nextBytes(8);
                $hexEncoded = bin2hex($bytes);
                $task->setState("Active" . $hexEncoded);
                $em->persist($task);
                $user->addTask($task);
            }

            $em->persist($user);
            $em->flush();

            $this->get('session')->getFlashBag()->add('info', 'Task: ' . $task->getId() . 'created!!!');
            return $this->redirect($this->generateUrl('curso_bastar_do_task'));
        }

        return $this->render('CursoBastarDoBundle:Default:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function ajaxAction(Request $request)
    {

       // $em = $this->getDoctrine()->getManager();
       // $generator = $this->container->get('security.secure_random');

        if ($request->isXmlHttpRequest()) {

            $nameuser = $request->query->get('name');
            $numTasks = $request->query->get('tasks');

            try {

                $this->get("curso_bastar_do.task")->persistUser( $nameuser, $numTasks );

//                $user = new User();
//                $user->setName($nameuser);
//
//                for ($i = 0; $i < $numTasks; $i++) {
//                    $task = new Task();
//                    $task->setTime(new \DateTime());
//
//                    // ramdom number 8bytes
//                    $bytes = $generator->nextBytes(8);
//                    $hexEncoded = bin2hex($bytes);
//                    $task->setState("Active" . $hexEncoded);
//                    $em->persist($task);
//                    $user->addTask($task);
//                }
//
//                $em->persist($user);
//                $em->flush();
            } catch (\Exception $e) {
                $response = new Response($e->getMessage(), 404);
                return $response;
            }

            $response = new Response("Todo OK", 200);
            //$response->headers->set('Content-Type', 'application/json');

            return $response;
        }
        die();
    }

}
