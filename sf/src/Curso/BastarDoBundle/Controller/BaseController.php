<?php

namespace Curso\BastarDoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TaskController extends Controller
{
    public function indexAction()
    {
        $tasks = array( 'Task0' =>array( 'time' => date("H:i:s"), 'state' => 'paused', 'link' => 0),
            'Task1' =>array( 'time' => date("H:i:s"), 'state' => 'paused', 'link' => 1),
            'Task2' =>array( 'time' => date("H:i:s"), 'state' => 'paused', 'link' => 2),
            'Task3' =>array( 'time' => date("H:i:s"), 'state' => 'paused', 'link' => 3) );
        return $this->render('CursoBastarDoBundle:Default:index.html.twig', array('tasks' => $tasks));
    }
    
    public function taskAction()
    {


    }

    public function viewAction() {

    }
}
