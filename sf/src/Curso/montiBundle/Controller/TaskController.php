<?php

namespace Curso\montiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TaskController extends Controller
{
    private $tasks = array(
        1 => array('nombre' => 'Recuento', 'estado' => 'ok'),
        2 => array('nombre' => 'entrevistas', 'estado' => 'Ko'),
        3 => array('nombre' => 'Visitas', 'estado' => 'Pendent'));

    public function indexAction()
    {
        return $this->render('CursomontiBundle:Default:index.html.twig', 
            array('name' =>'Monti', 'tasks' => $this->tasks));
    }

    public function viewAction($id)
    {
        return $this->render('CursomontiBundle:Default:view.html.twig', 
            array('id' => $id));
    }
   
}