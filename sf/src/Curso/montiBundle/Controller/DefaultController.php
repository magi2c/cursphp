<?php

namespace Curso\montiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('CursomontiBundle:Default:index.html.twig', array('name' => $name));
    }

    public function adiosAction($name)
    {
        return $this->render('CursomontiBundle:Default:adios.html.twig', array('name' => $name));
    }
    public function usuariAction($name)
    {
        return $this->render('CursomontiBundle:Default:usuari.html.twig', array('name' => $name));
    }
}
