<?php

namespace Curso\montiBundle\Command;

use Curso\ToDoBundle\Entity\Task;
use Curso\ToDoBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class montiCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('curso:monti:create')
            ->setDescription('Create tasks')
            ->addArgument('numT', InputArgument::REQUIRED, 'Numero de taskcas creadas')
            ->addArgument('nomUsu', InputArgument::REQUIRED, 'Numero de taskcas creadas')
            ->addArgument('emailUsu', InputArgument::REQUIRED, 'Numero de taskcas creadas')            
            ->addOption('sufix', 'u', InputOption::VALUE_OPTIONAL, 'Sufix task')
            ->setHelp(<<<EOF
The <info>%command.name%</info> :

<info>php %command.full_name%</info>

Inserta task en la bd


<info>php %command.full_name%</info> 10
EOF
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $numTask = $input->getArgument('numT');
        $nomUsuari=$input->getArgument('nomUsu');
        $email=$input->getArgument('emailUsu');

        $em = $this->getContainer()->get('doctrine')->getManager();

        $repository = $em->getRepository('CursoToDoBundle:User');
        $user = $repository->findByEmail($email);
        
        if(!$user){
            $output->writeln('NO esta: '.$email . ' Result:');
        }else{
            //var_dump($user);
            $output->writeln('Esta: '.$email .' Result: Usuari existent' . $user->getId());
        }

/*
        $output->writeln('Creamos <comment>'.$numTask.'</comment>!...'.$input->getOption('sufix'));

        $user = new User();
        $user->setEmail($email);
        $user->setNombre($nomUsuari);
        $em->persist($user);

        $task = new Task();
        $task->setNombre($numTask);
        $task->setUser($user);
        $task->setData(new \DateTime());
        $em->persist($task);

        $em->flush();
*/
    }
    
}
