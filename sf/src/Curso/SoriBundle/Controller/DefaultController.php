<?php

namespace Curso\SoriBundle\Controller;



use Curso\ToDoBundle\Entity\Task;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{


private $tasks=array();
private $usuari ="Carlos Soriano";
private $pag_ini=0;
private $pag_fin=6;

        

    public function indexAction($name)
    {
        return $this->render('CursoSoriBundle:Default:index.html.twig', array('name' => $name));
    }

     public function hola2Action($slug)
    {
        die($slug);
    }


    public function tasksAction() {

     $bastardo = $this->get('translator')->trans('TRADUCCIÓN???');

       $em=$this->container->get('doctrine')->getManager();
       $repository = $em->getRepository('CursoToDoBundle:Task');
       $this->tasks=$repository->findAll();

        return $this->render('CursoSoriBundle:Default:index_tasks.html.twig',array(
                'tasks'=>$this->tasks,
                'usuari'=>$this->usuari,
                'pag_ini'=>0,
                'pag_fin'=>6,
                'bastardo' => $bastardo
        ));



    /*	$this->tasks[0]=array("nom"=> "Bastardo1","hora"=>"9.00", "estat"=>"Fet", "link"=>"1", "description"=>"Descripción Tasca bastarda1");
    	$this->tasks[1]=array("nom"=> "Bastardo2","hora"=>"10.00", "estat"=>"Pendent", "link"=>"2",  "description"=>"Descripción Tasca bastarda2");
    	$this->tasks[2]=array("nom"=> "Bastardo3","hora"=>"11.00", "estat"=>"Fet", "link"=>"3", "description"=>"Descripción Tasca bastarda3");


    	return $this->render('CursoSoriBundle:Default:index_tasks.html.twig', array('tasks' => $this->tasks));*/
    }

 /*public function tasks_viewAction($id) {

 	
    $this->tasks[0]=array("nom"=> "Bastardo1","hora"=>"9.00", "estat"=>"Fet", "link"=>"1", "description"=>"Descripción Tasca bastarda1");
    $this->tasks[1]=array("nom"=> "Bastardo2","hora"=>"10.00", "estat"=>"Pendent", "link"=>"2", "description"=>"Descripción Tasca bastarda2");
    $this->tasks[2]=array("nom"=> "Bastardo3","hora"=>"11.00", "estat"=>"Fet", "link"=>"3", "description"=>"Descripción Tasca bastarda3");


    
    	return $this->render('CursoSoriBundle:Default:index_task.html.twig', array('task' => $this->tasks[$id]));
    }*/


   /* public function newAction(Request $request)
    {
       
        // crea una task y le asigna algunos datos ficticios para este ejemplo
        $task = new Task();
        
        $form = $this->createFormBuilder($task)
            ->add('nombre', 'text')
            ->add('data', 'date')
            ->add('save', 'submit')
            ->getForm();

        return $this->render('CursoSoriBundle:Default:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }*/


    public function tasks_viewAction($id)
    {
        $em=$this->container->get('doctrine')->getManager();
        $repository = $em->getRepository('CursoToDoBundle:Task');
        $task=$repository->findOneById($id);

        return $this->render('CursoSoriBundle:Default:index_task.html.twig',array(
            'usuari'=>$this->usuari,
            'task'=>$task
        ));
    }


 public function tasks_editAction($id, Request $request)
    {
        $em=$this->container->get('doctrine')->getManager();
        $repository = $em->getRepository('CursoToDoBundle:Task');
        $task=$repository->findOneById($id);

         $form = $this->createFormBuilder($task)
            ->add('nombre', 'text')
            ->add('data', 'date')
            ->add('edit', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            // guardar la tarea en la base de datos
            $em=$this->container->get('doctrine')->getManager();
            $repository = $em->getRepository('CursoToDoBundle:Task');
            $em->persist($task);
            $em->flush();

            $this->get('session')->getFlashBag()->add('info','La tasca amb ID'. $task->getId() . 's\'ha editat correctament!');

            //$this->get('session')->
            return $this->redirect($this->generateUrl('curso_sori_tasks'));
        }


        return $this->render('CursoSoriBundle:Default:new.html.twig', array(
            'form' => $form->createView(),
            'usuari'=>$this->usuari,
        ));

       /* return $this->render('CursoSoriBundle:Default:index_task.html.twig',array(
            'usuari'=>$this->usuari,
            'task'=>$task
        ));*/
    }

    public function newAction(Request $request)
    {
        // crea una task y le asigna algunos datos ficticios para este ejemplo
        $task = new Task();

        $form = $this->createFormBuilder($task)
            ->add('nombre', 'text')
            ->add('data', 'date')
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
             $em=$this->container->get('doctrine')->getManager();
            // guardar$em=$this->container->get('doctrine')->getManager(); la tarea en la base de datos
             $this->get('curso_sori_to_do.example')->persistTask($task);
           /* $em=$this->container->get('doctrine')->getManager();
            $repository = $em->getRepository('CursoToDoBundle:Task');
            $em->persist($task);
            $em->flush();*/

               $this->get('session')->getFlashBag()->add('info','La tasca amb ID'. $task->getId() . 's\'ha creat correctament!');

     $message = \Swift_Message::newInstance()
        ->setSubject('Nova tasca')
        ->setFrom('new_task@example.com')
        ->setTo('csoriano@tarragona.cat')
        ->setBody( 'La tasca amb ID'. $task->getId() . 's\'ha creat correctament!');
    $this->get('mailer')->send($message);
           // return $this->redirect($this->generateUrl('curso_sori_new_tasksuccess'));
             return $this->redirect($this->generateUrl('curso_sori_tasks'));
        }


        return $this->render('CursoSoriBundle:Default:new.html.twig', array(
            'form' => $form->createView(),
            'usuari'=>$this->usuari,
        ));

    }


    public function newsuccessAction()
    {
        return $this->render('CursoSoriBundle:Default:newsuccess.html.twig',array(
            'usuari'=>$this->usuari
        ));
    }

    public function editsuccessAction()
    {
        return $this->render('CursoSoriBundle:Default:editsuccess.html.twig',array(
            'usuari'=>$this->usuari
        ));
    }


   
}

