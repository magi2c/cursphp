<?php

namespace Curso\SoriBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tag
 *
 * @ORM\Table(name="csp_tag")
 * @ORM\Entity()
 */
class Tag
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



 /**
     * @var string
     *
     * @ORM\Column(name="nom_tag", type="string", length=255, unique=false)
     */
    private $nom_tag;


     /**
     * @ORM\ManyToMany(targetEntity="Task", mappedBy="tags")
     */
    private $tasks;

    public function __construct() {
        $this->tasks = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }



     /**
     * Set nom_tag
     *
     * @param string $nom_tag
     * @return Tag
     */
    public function setNomTag($nom_tag)
    {
        $this->nom_tag = $nom_tag;

        return $this;
    }

    /**
     * Get nom_tag
     *
     * @return string 
     */
    public function getNomTag()
    {
        return $this->nom_tag;
    }
}
