<?php

namespace Curso\SoriBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * @ORM\Table(name="csp_user")
 * @ORM\Entity()
 * @UniqueEntity("email")
 */



class User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

 /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, unique=false)
     */
    private $nombre;


 /**
     * @ORM\OneToMany(targetEntity="Task", mappedBy="user")
     * solo si no es por defecto:
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $tasks;


    /**
     * @return mixed $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * @param mixed $tasks
     */
    public function setTasks($tasks)
    {
        $this->tasks = $tasks;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    public function __toString()
    {
        return "ID:\t\t".$this->getId()."\nNombre:\t\t".$this->getNombre()."\nEmail:\t\t".$this->getEmail()."\n";
    }

}
