<?php
namespace Curso\SoriBundle\Command;

use Curso\SoriBundle\Entity\User;
use Curso\SoriBundle\Entity\Tag;
use Curso\SoriBundle\Entity\Task;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class CreateTaskCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('curso:createSoriTask')
            ->setDescription('Create tasks')
            ->addArgument('num', InputArgument::REQUIRED, 'Numero de taskcas creadas')
            ->addArgument('nom_user', InputArgument::REQUIRED, 'Usuario')
             ->addArgument('email_user', InputArgument::REQUIRED, 'Email')
            ->addOption('sufix', 'u', InputOption::VALUE_OPTIONAL, 'Sufix task')
            ->setHelp(<<<EOF
The <info>%command.name%</info> :

<info>php %command.full_name%</info>

Inserta task en la bd


<info>php %command.full_name%</info> 10
EOF
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $numTask = $input->getArgument('num');
        $nomUser = $input->getArgument('nom_user');
        $emailUser = $input->getArgument('email_user');

        $em = $this->getContainer()->get('doctrine')->getManager();


        $output->writeln('Creamos <comment>'.$numTask.'</comment>!...'.'Para el usuario <comment>'.$nomUser.'</comment>!'.$input->getOption('sufix'));


          $repository = $em->getRepository('CursoSoriBundle:User');
          $user = $repository->findOneByNom($nomUser);

          if ($user == NULL) {
           

        $user = new User();
       $user->setNom($nomUser);
       $user->setEmail($emailUser);
        $em->persist($user);
         $em->flush();
          var_dump('USER NULL: CREANDO USUARIO'. $user->getNom());
        }

        else {

           var_dump('YA EXISTE '.$user->getNom());
       }
         // $repository_task = $em->getRepository('CursoSoriBundle:Task');
         // $repository_tag =  $em->getRepository('CursoSoriBundle:Tag');
        
        

         $task = new Task();
        $task->setNom('test');
        var_dump('CREANDO TASCA'. $task->getNom().' PARA EL USUARIO '.$user->getNom());
        
        $task->setUser($user);
        var_dump('USUARIO DE LA TASCA: '.$task->getUser()->getNom());

        //$task->setData(new \DateTime());
        $em->persist($task);
        var_dump('PERSIST'.$task->getUser()->getNom());
         var_dump('CREO LA TASCA '.$task->getNom());
          $em->flush();
        var_dump('FLUSH CREO LA TASCA'.$task->getNom());
       
         //  for ($i=0;$i<$numTask;$i++) {

              

          // }

        

       

      /*  $repository = $em->getRepository('CursoSoriBundle:User');


        $task = new Task();
        $task->setNombre('test');
        $task->setUser($user);
        //$task->setData(new \DateTime());
        $em->persist($task);

        $em->flush();*/

     

    }
}

