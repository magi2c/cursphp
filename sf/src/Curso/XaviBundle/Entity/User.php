<?php

namespace Curso\XaviBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Xavi User
 *
 * @ORM\Table(name="xavi_user")
 * @ORM\Entity()
 */
class User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @param $email string
     * @param $name string
     */
    public function setAll( $email, $name )
    {
        $this->$email = $email;
        $this->name  = $name;
    }

    /* ====== SETTERS & GETTERS ============ */
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $nom
     */
    public function setName($nom)
    {
        $this->name = $nom;
    }

}
