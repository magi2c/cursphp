<?php

namespace Curso\XaviBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="xavi_task")
 * @ORM\Entity()
 */
class Task
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\Email(message="Posa un email!")
     */
    private $name;

    /**
     * @var date
     *
     * @ORM\Column(name="init_date", type="date", nullable=true)
     */
    private $initDate;

    /**
     * @var date
     *
     * @ORM\Column(name="end_date", type="date", nullable=true)
     */
    private $endDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="completion", type="integer")
     * @Assert\Regex(pattern= "/[0-9]/",message="Nomes números")
     * @Assert\NotBlank
     */
    private $completion;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $user;

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @var Tag
     *
     * @ORM\ManyToMany(targetEntity="Tag")
     * @ORM\JoinTable(name="xavi_task_tag")
     */
    private $tags;

    public function _construct()
    {
        $this->tags = new ArrayCollection();
    }

    public function setAll( $name, $completion )
    {
        $this->name = $name;
        $this->completion = $completion;
    }
    /* ===== SETTERS AND GETTERS ====== */

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return date
     */
    public function getInitDate()
    {
        return $this->initDate;
    }

    /**
     * @param date $initDate
     */
    public function setInitDate($initDate)
    {
        $this->initDate = $initDate;
    }

    /**
     * @return date
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param date $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @return int
     */
    public function getCompletion()
    {
        return $this->completion;
    }

    /**
     * @param int $completion
     */
    public function setCompletion($completion)
    {
        $this->completion = $completion;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


}
