<?php

namespace Curso\XaviBundle\Controller;

use Curso\XaviBundle\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class TaskController extends Controller
{

    /**
     * @return Repository Torna el repositori de Task
     */
    public function getRepoTask()
    {
        return( $this->getDoctrine()->getManager()->getRepository('CursoXaviBundle:Task') );
    }

    private function llistaUsuaris()
    {
        //Repositori de tasques
        $repoTask = $this->getRepoTask();

        return  $repoTask->findAll();
    }

    public function indexAction(  )
    {
        return $this->render('CursoXaviBundle:Task:task.html.twig', array( 'llista' => $this->llistaUsuaris() ) );
    }

    public function viewAction( $id )
    {
        //Repositori de tasques
        $repoTask = $this->getRepoTask();

        $task = $repoTask->findOneById( $id );

        return $this->render('CursoXaviBundle:Task:view.html.twig', array( 'task' => $task ) );
    }

    /**
     * Presenta el formulari per afegir una tasca nova
     */
    public function editAction( Request $request )
    {
        $task = new Task();

        $form = $this->createFormBuilder($task)
                            ->add('name', 'text', array('label' => "Nom: "))
                            ->add('completion', 'text', array('label' => "% completat: "))
                            ->add('save', 'submit', array('label' => "Crear tasca"))
                            ->getForm();

        //Comprovem si s'ha enviat el formulari
        $form->handleRequest($request);

        if( $form->isValid() )
        {
            $task = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist( $task );
            $em->flush();

            //Introducimos la sessión para mostrar mensajes de confirmación
            $this->get('session')->getFlashBag()->add(
                'success',
                'La tasca amb ID' . $task->getId() . "s'ha creat correctament."
            );
            return $this->redirect( $this->generateUrl( 'task' ) );
        }

        return $this->render('CursoXaviBundle:Task:edit.html.twig', array(
                                                                        'form' => $form->createView()
                                                                         )
                            );
    }

    /**
     * Controlador per servir AJAX
     */
    public function refreshAction( Request $request )
    {
        $tasks = $this->llistaUsuaris();
        $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));

        $response = new Response($serializer->serialize($tasks, 'json'));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function sendAction( Request $request )
    {
        //Repositori de tasques
        $contenido = 'Hello';
        $mensaje = \Swift_Message::newInstance('Hello',$contenido)
                                      ->setFrom('xreina@tarragona.cat')
                                      ->setTo('xreina@tarragona.cat');

        $this->get('mailer')->send($mensaje);
        return $this->render('CursoXaviBundle:Task:task.html.twig', array( 'llista' => $this->llistaUsuaris() ) );
    }
}
