<?php

namespace Curso\XaviBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class XaviFirstController extends Controller
{
    public function indexAction()
    {
        return $this->render('CursoXaviBundle:First:index.html.twig');
    }
}
