<?php

namespace Curso\XaviBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('CursoXaviBundle:Default:index.html.twig', array('name' => $name));
    }
}
