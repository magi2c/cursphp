<?php

namespace Curso\XaviBundle\Command;

use Curso\XaviBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class AddUserCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('xavi:addUser')
            ->setDescription('Afegeix usuaris')
            ->setDefinition( array(
                                    new InputArgument(  'email',
                                                        InputOption::VALUE_REQUIRED,
                                                        'Email de l\'usuari'
                                                   ),
                                    new InputOption(   'nom',
                                                        null,
                                                        InputOption::VALUE_REQUIRED,
                                                        'Nom de l\'usuari'
                                                     ),
                                  )
                            )
            ->setHelp(<<<EOF
Introdueix l'usuari que es passa com a paràmetre

<info>php %command.full_name%</info>
EOF
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //Comprovem si existèix el paràmetre obligatori
        if( is_null( $input->getArgument('email') ) ||  is_null( $input->getOption('nom') ) )
        {
            $output->writeln('[ERROR]: no s\'ha proporcionat nom o email');
            $output->write( $this->getHelp() );
        }
        else
        {
            $user = new User();
            $user->setEmail( $input->getArgument('email') );

            $user->setName( $input->getOption('nom') );

            //persisteix
            $em = $this->getContainer()->get('doctrine')->getManager();

            $em->persist( $user );
            $em->flush();


            $output->writeln( "Usuari creat. Nom: ".$user->getName().", email: ".$user->getEmail().", id: ". $user->getId() );
        }


    }
}
