<?php

namespace Curso\XaviBundle\Command;

use Curso\XaviBundle\Entity\Task;
use Curso\XaviBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class PopulateDbCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('xavi:populate')
            ->setDescription('Populate database with random tasks')
            ->setDefinition( array(
                                     new InputOption(   'userName',
                                                        'u',
                                                        InputOption::VALUE_REQUIRED,
                                                        'Nom de l\'usuari a qui s\'assigna la tasca (opcional)' )
                                  )
                            )
            ->setHelp(<<<EOF
Introdueix tasques aleatories a la base de dades. S'assigna la tasca a l'usuari que es passa com a paràmetre.
EOF
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $task = new Task();

        $task->setName( substr(md5(microtime()),rand(0,26),5) );
        $task->setCompletion( 0 );

        //assigna la tasca a un usuari: obtenim usuari
        $em = $this->getContainer()->get('doctrine')->getManager();
        $repo = $em->getRepository('CursoXaviBundle:User');

        //Comprova si han passat usuari
        $user = null; //sense usuari
        if( !is_null($input->getOption('userName')) )
            $user =  $repo->findOneByName( $input->getOption('userName') );

        $task->setUser( $user );

        //persisteix
        $em->persist( $task );
        $em->flush();

        $output->writeln( "Afegida tasca amb nom:".$task->getName().", id: ".$task->getId() );
    }
}
